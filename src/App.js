import "./App.css";
import { Routes, Route } from "react-router-dom";

import Layout from "./Components/Layout";
import Home from "./routes/Home";
import About from "./routes/About";
import Contact from "./routes/Contact";
import Dashboard from "./routes/Dashboard";

import Courses, { Course } from "./routes/Courses";

import { AuthProvider } from "./Components/auth";
import Login from "./routes/Login";
import RequireAuth from "./Components/RequireAuth";

function App() {
  return (
    <AuthProvider>
      <div className="App">
        <Routes>
          <Route path="/" element={<Layout />}>
            <Route index element={<Home />} />
            <Route path="courses" element={<Courses />}>
              <Route index element={<h4>Select Course</h4>} />
              <Route path=":id" element={<Course />} />
            </Route>
            <Route path="about" element={<About />} />
            <Route path="contact" element={<Contact />} />
            <Route path="dashboard" element={<RequireAuth><Dashboard /></RequireAuth>} />
            <Route path='login' element={<Login />} /> 
          </Route>
          <Route path="*" element={<h1>Not Found!</h1>} />
        </Routes>
      </div>
    </AuthProvider>
  );
}

export default App;
