import { Link, Outlet, useNavigate } from "react-router-dom";
import { useAuth } from "./auth";

const Layout = () => {
  const navigate = useNavigate();
  const auth = useAuth();

  const goBack = () => {
    navigate(-1);
  };

  return (
    <div>
      <div
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
          width: "80%",
          margin: "0 auto",
        }}
      >
        <div
          style={{
            display: "flex",
            alignItems: "center",
          }}
        >
          <button
            style={{
              height: "25px",
              marginRight: "5px",
            }}
            onClick={goBack}
          >
            Go Back
          </button>
          <h2>Logo</h2>
        </div>
        <nav>
          <Link to="/" style={{ marginRight: "10px", textDecoration: "none" }}>
            Home
          </Link>
          <Link
            to="courses"
            style={{ marginRight: "10px", textDecoration: "none" }}
          >
            Courses
          </Link>
          <Link
            to="about"
            style={{ marginRight: "10px", textDecoration: "none" }}
          >
            About
          </Link>
          <Link
            to="contact"
            style={{ marginRight: "10px", textDecoration: "none" }}
          >
            Contact
          </Link>
          <Link
            to="/dashboard"
            style={{ marginRight: "10px", textDecoration: "none" }}
          >
            Dashboard
          </Link>
          {!auth.user && (
            <Link
              to="/login"
              style={{ marginRight: "10px", textDecoration: "none" }}
            >
              Login
            </Link>
          )}
        </nav>
      </div>
      <hr />
      <Outlet />
    </div>
  );
};

export default Layout;
