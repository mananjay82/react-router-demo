import { useAuth } from "./auth";
import { Navigate, useLocation } from "react-router-dom";

const RequireAuth = (props) => {
    const auth = useAuth();
    const location = useLocation();

    if(!auth.user){
        return <Navigate to='/login' state={{ path: location.pathname }} />;
    }

    return props.children;

}

export default RequireAuth;