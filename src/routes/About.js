import {useNavigate} from 'react-router-dom';

const About = () => {
    const navigate = useNavigate();
    return (
        <div>
            <h1>About Component</h1>
            <button onClick={() => {navigate('/contact', {state: 'Navigated from about component'})}}>Go to Contact</button>
        </div>
    )
}


export default About;