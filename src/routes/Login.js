import { useState } from "react"
import { useAuth } from "../Components/auth";
import { useNavigate, useLocation } from 'react-router-dom';

const Login = () => {
    const [user, setUser] = useState('');
    const auth = useAuth();
    const navigate = useNavigate();
    const location = useLocation();

    const redirectPath = location.state?.path || "/"

    const handleLogin = () => {
        auth.login(user)
        navigate(redirectPath, { replace: true });
    };

    return (
        <div>
            Username: <input type='text' value={user} onChange={(event) => {setUser(event.target.value)}} />
            <button onClick={handleLogin}>Login</button>
        </div>
    )
}

export default Login;
