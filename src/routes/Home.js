import { useAuth } from "../Components/auth";


const Home = () => {
    const auth = useAuth();

    return (
        <>
            <h1>Home Component {auth.user && <span>Welcome {auth.user} you are logged in. </span>} </h1>
        </>
    )
}

export default Home;