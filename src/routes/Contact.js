import { useLocation } from 'react-router-dom';

const Contact = () => {
    const location = useLocation();
    return <h1>Contact Component {" "} {location.state ? <span>{location.state}</span>: " "} </h1>
}

export default Contact;