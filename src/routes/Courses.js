
import {Link, Outlet, useParams, useSearchParams} from 'react-router-dom';

const CoursesData = [
    {
        id: 1,
        name: 'HTML',
        content: 'The HyperText Markup Language or HTML is the standard markup language for documents designed to be displayed in a web browser. It can be assisted by technologies such as Cascading Style Sheets and scripting languages such as JavaScript.'
    },
    {
        id: 2,
        name: 'CSS',
        content: 'Cascading Style Sheets is a style sheet language used for describing the presentation of a document written in a markup language such as HTML or XML. CSS is a cornerstone technology of the World Wide Web, alongside HTML and JavaScript'
    },
    {
        id: 3,
        name: 'JavaScript',
        content: 'JavaScript, often abbreviated JS, is a programming language that is one of the core technologies of the World Wide Web, alongside HTML and CSS. As of 2022, 98% of websites use JavaScript on the client side for web page behavior, often incorporating third-party libraries.'
    },
    {
        id: 4,
        name: 'ReactJs',
        content: 'React is a free and open-source front-end JavaScript library for building user interfaces based on UI components. It is maintained by Meta and a community of individual developers and companies.'
    },
];

const Courses = () => {
  const [searchParams, setSearchParams] = useSearchParams()  
  const searchValChangeHandler = (event) => {
    let course = event.target.value;
    if(course)
        setSearchParams({course});
    else   
        setSearchParams({});
  }
  return (
    <div>
        <h1>All Courses</h1>
        <input type='text' value={searchParams.get("course") || ""} onChange={searchValChangeHandler} placeholder='Search Courses' />
        {
            CoursesData.filter((course) => {
                let filter = searchParams.get('course');
                if(!filter) return true;
                let name = course.name.toLowerCase();
                return name.startsWith(filter.toLowerCase());
            }).map((course) => (
                <h4 key={course.id}><Link to={`${course.id}`}>{course.name}</Link></h4>
            ))
        }
        <hr />
        <Outlet />
    </div>
  )
}

export const Course = () => {
    const params = useParams();
    const course = CoursesData.find((course) => course.id === parseInt(params.id));
    return (
        <div style={{
            width: '60%',
            margin: '0 auto'
        }}>
            <h2>{course.name}</h2>
            <p>{course.content}</p>
        </div>
    )
}

export default Courses;